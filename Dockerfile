FROM alpine:3.15 as build

ENV HUGO_VERSION=0.96.0 \
    HUGO_SITE=/srv/hugo

RUN apk --no-cache add \
    curl \
    git \
    && curl -SL https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz \
    -o /tmp/hugo.tar.gz \
    && tar -xzf /tmp/hugo.tar.gz -C /tmp \
    && mv /tmp/hugo /usr/local/bin/ \
    && apk del curl \
    && mkdir -p ${HUGO_SITE} \
    && rm -rf /tmp/*

WORKDIR ${HUGO_SITE}

COPY . ${HUGO_SITE}

RUN hugo

FROM tdewolff/minify:latest as minify

COPY --from=build /srv/hugo/public /srv/hugo/public
WORKDIR /srv/hugo/public

RUN minify --recursive --verbose \
        --match=\.*.js$ \
        --type=js \
        --output ./ \
        .

RUN minify --recursive --verbose \
        --match=\.*.css$ \
        --type=css \
        --output ./ \
        .

RUN minify --recursive --verbose \
        --match=\.*.html$ \
        --type=html \
        --output ./ \
        .

FROM nginx:alpine

COPY --from=minify /srv/hugo/public /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
