---
title: "Introduction"
date: 2022-05-01T20:44:50+02:00
#weight: 1
# aliases: [""]
tags: [""]
# author: "Me"
showToc: false
draft: false
hidemeta: false
description: "Introduction about blog.letnh.dev"
# canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
# disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
---

# Welcome to blog.letnh.dev

Currently this website is under development, updates will come in the furture.

In the furture I will cover topics such as:
- Programming
- Infrastructure _(Linux, Networking, etc)_
- Security
- Automation 

and more so stay tuned.

If you find this intressting, please check back at a later time or subscribe to the [RSS Feed](https://blog.letnh.dev/index.xml)
